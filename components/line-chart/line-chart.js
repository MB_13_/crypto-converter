import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default {
  extends: Line,
  mixins: [reactiveProp],
  data(){
      return {
        options: {
            responsive: true,
            maintainAspectRatio: false,
            aspectRatio: 3,
            interaction: {
                intersect: false
            },
            radius: 0,
        }
      }
  },
  mounted () {
    // this.chartData создаётся внутри миксина.
    // Если вы хотите передать опции, создайте локальный объект options
    this.renderChart(this.chartData, this.options)
  }
}