export const state = () => ({
  current: {},
  btc: 1,
  eth: 1,
})

export const mutations = {
  set(state, data) {
    state.current = data;
  },
  addCoin(state, coin) {
    state[coin.name] = state[coin.name] + coin.count;
  },
  removeCoin(state, coin) {
    if(coin.count > state[coin.name]){
      state[coin.name] = 0;
    } else {
      state[coin.name] = state[coin.name] - coin.count;
    }
    
  }
}

export const actions = {
  getCurCourse(context) {
    this.$axios.get('https://api.coingecko.com/api/v3/simple/price?ids=bitcoin%2Cethereum%2Cusd-coin&vs_currencies=btc%2Ceth%2Cusd')
      .then(response => {
        context.commit('set', response.data)
        this.current['usd-coin'].usd = 1;
      })
      .catch(error => error)
  },
  changePortfolio(context,coin){
    if(coin.operation){
      context.commit('addCoin',coin)
    } else {
      context.commit('removeCoin',coin)
    }
  }
}